#!/bin/perl
##############################################################################################
# Copyright (c) Microsoft
#
# All rights reserved
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file 
# except in compliance with the License. You may obtain a copy of the License at 
# http://www.apache.org/licenses/LICENSE-2.0  
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER 
# EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF 
# TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT. 
#
# See the Apache Version 2.0 License for specific language governing permissions and 
# limitations under the License.
##############################################################################################
use Cwd;
use Cwd 'abs_path';
use File::Basename;
use File::Find;
use Safe;
use strict;

### Compiler configuration
my $cflag_define = '-D%s=%s'; # to be used as sprintf($cflag_define, "NAME", "VALUE");
my $test_exec = './test.out';

### Environment configuration
my $tests_root = './Tests';

if(defined($ENV{"TEST_ROOT"}))
{
    $tests_root = $ENV{"TEST_ROOT"};
}

my $run_log = 'run.log';

#Change following environment variables to your seeting
my $CLANG_AMP_HOME="/home/alex/cppamp/src"; # where you checkout source
my $CLANG_AMP_BUILD_DIR="/home/alex/cppamp/release"; # where you build

#OpenCL settings
# For NVIDIA
#my $OPENCL_INC="/usr/include";
# For ATI
my $OPENCL_INC="/opt/AMDAPP/include";

#or set by environment variables
if(defined($ENV{"CLANG_AMP_HOME"}))
{
    $CLANG_AMP_HOME = $ENV{"CLANG_AMP_HOME"};
}
if(defined($ENV{"CLANG_AMP_BUILD_DIR"}))
{
    $CLANG_AMP_BUILD_DIR = $ENV{"CLANG_AMP_BUILD_DIR"};
}
if(defined($ENV{"OPENCL_INC"}))
{
    $OPENCL_INC = $ENV{"OPENCL_INC"};
}

my $CLANG_AMP="$CLANG_AMP_BUILD_DIR/compiler/bin/clang++";
my $CLAMP_CONFIG=`find $CLANG_AMP_BUILD_DIR/build -name clamp-config -print`;
$CLAMP_CONFIG =~ s/^\s+//;
$CLAMP_CONFIG =~ s/\s+$//;
my $CLAMP_CXXFLAGS=`$CLAMP_CONFIG --build --cxxflags`;
$CLAMP_CXXFLAGS =~ s/^\s+//;
$CLAMP_CXXFLAGS =~ s/\s+$//;
my $CLAMP_LDFLAGS=`$CLAMP_CONFIG --build --ldflags`;
$CLAMP_LDFLAGS =~ s/^\s+//;
$CLAMP_LDFLAGS =~ s/\s+$//;
my $CLAMP_DEVICE="$CLANG_AMP_BUILD_DIR/lib/clamp-device";
my $CLAMP_EMBED="$CLANG_AMP_BUILD_DIR/lib/clamp-embed";
my $SHARED_CXXFLAGS="$CLAMP_CXXFLAGS -Iamp_test_lib/inc -I$OPENCL_INC";

### Execute tests
use constant PASS => 0;
use constant SKIP => 1;
use constant FAIL => 2;
my $num_passed = 0;
my $num_skipped = 0;
my $num_failed = 0;

my $test=@ARGV[0];

print "Test: $test\n";

# Read test configuration
undef %Test::config;

my $conf_file = try_find_file_by_extension(abs_path(dirname(($test))), abs_path($tests_root), "conf");
if ($conf_file = -1) { exit (1) };

if(-e $conf_file)
{
    my $safe = new Safe('Test');
    $safe->rdo($conf_file) or &exit_message(1, "Cannot open $conf_file");
}

if(not defined $Test::config{'definitions'})
{
    $Test::config{'definitions'} = [{}];
}

# Find "expects error" directives in cpp
open(TEST_CPP, $test) or &exit_message(1, "Cannot open $test");
$Test::config{'expected_success'} = (grep m@//#\s*Expects\s*(\d*)\s*:\s*(warning|error)@i, <TEST_CPP>) == 0;
close(TEST_CPP);

print ('Compile only: '.bool_str($Test::config{'compile_only'})."\n"
    .'Expected success: '.bool_str($Test::config{'expected_success'})."\n");

# check to see if test has its own main
# This solution taken from https://github.com/pathscale/amp-testsuite/commit/1f9f186d27446e52bd50dbcf429844f3fa308303
my $include_main='';
if (! system ("cat $test | grep ' main *(' > /dev/null")) {
    $include_main='-include test_main.h';
}

# For each set of definitions
foreach my $def_set (@{$Test::config{'definitions'}})
{
    print "$test:\n";

    # Build and execute test
    my $cflags_defs = '';
    while(my ($k, $v) = each(%{$def_set}))
    {
        $cflags_defs = $cflags_defs.sprintf($cflag_define.' ', $k, $v);
    }
    my $command;
    if ($Test::config{'compile_only'}) {
        $command = "\\
            $CLANG_AMP -cc1 -fsyntax-only $SHARED_CXXFLAGS $include_main $test  $cflags_defs -D__CPU__=1";
    } else {
        $command = "\\
            $CLANG_AMP $SHARED_CXXFLAGS $include_main $test -fno-common -m32 $cflags_defs -D__GPU__=1 -Xclang -fcuda-is-device -emit-llvm -c -S -O2 -o $test.ll && \\
            $CLAMP_DEVICE $test.ll kernel.cl && \\
            $CLAMP_EMBED kernel.cl kernel.o && \\
            $CLANG_AMP $SHARED_CXXFLAGS $include_main $test amp_test_lib/lib/libamptest.o $cflags_defs -D__CPU__=1 $CLAMP_LDFLAGS kernel.o -o test.out -g";
    }
    
    print ("Command: $command\n"
        ."Build output:\n"
        ."<<<\n");
    my $build_exit_code = system($command) >> 8;
    print (">>>\n"
        ."Build exit code: $build_exit_code\n");

    my $exec_exit_code;
    if((not $Test::config{'compile_only'}) && $build_exit_code == 0 && $Test::config{'expected_success'})
    {
        print ("Execution output:\n"
            .'<<<'."\n");
        $exec_exit_code = system("$test_exec") >> 8;
        print (">>>\n"
            ."Execution exit code: $exec_exit_code\n");
    }

    # Interpret result
    my $result;
    if(not $Test::config{'expected_success'}) # Negative test
    {
        if($build_exit_code != 0)
        {
            $result = PASS;
        }
        else
        {
            $result = FAIL;
        }
    }
    elsif($Test::config{'compile_only'}) # Compile only test
    {
        if($build_exit_code == 0)
        {
            $result = PASS;
        }
        else
        {
            $result = FAIL;
        }
    }
    else # Executable test
    {
        if($build_exit_code != 0)
        {
            $result = FAIL;
        }
        elsif($exec_exit_code == 0)
        {
            $result = PASS;
        }
        elsif($exec_exit_code == 2)
        {
            $result = SKIP;
        }
        else
        {
            $result = FAIL;
        }
    }
    
    if($result == PASS)
    {
        print ("Result: passed\n");
    }
    elsif($result == FAIL)
    {
        print ("Result: failed\n");
    }
    elsif($result == SKIP)
    {
        print ("Result: skipped\n");
    }
    else
    {
        exit_message(1, "Unexpected result!");
    }
}

### Subroutines
# Use: exit_message(code, msg)
sub exit_message
{
    if(@_ != 2) { die('exit_message expects 2 arguments'); }
    print("\n".($_[0] == 0 ? 'SUCCESS' : 'FAILURE').": ".$_[1]);
    exit($_[0]);
}

# Use: bool_str(val)
# Returns: string 'true'/'false'
sub bool_str
{
    return $_[0] ? 'true' : 'false';
}

## Use: get_files_by_extension($start_dir, $ext);
## Returns: List of files with given extension
sub get_files_by_extension($$)
{
    my $dir = $_[0];
    my $ext = $_[1];
    
	my @files = `ls $dir`;
    my @ext_files;
	
    for my $file (@files)
    {
		if($file =~ /\.$ext$/i)
		{
			chomp($file);
			push(@ext_files, $file);
		}
    }
    
    return @ext_files;    	
}

## Use: try_find_file_by_extension($start_dir, $end_dir, $ext);
## Returns: Relative path to file found. Empty if no file exists. -1 if error is encountered.
sub try_find_file_by_extension($$$)
{
	my $start_dir = $_[0];
	my $end_dir = $_[1];
	my $ext = $_[2];
	
	if(index($start_dir, $end_dir) == -1)
	{
		print "ERROR: $start_dir is not a subdirectory of $end_dir.\n";
		return -1;
	}
	
	my @files;

	do
	{	
		@files = get_files_by_extension($start_dir, $ext);
		
		if(@files > 1) 
		{
			print "Error: More than one (*.$ext) files present in directory $start_dir\n";
			return -1; 
		}
		
		if(@files != 0)
		{
			my $file = $files[0];
			
			if(-e "$start_dir/$file")
			{
				return "$start_dir/$file";
			}
		}
		
		# Move to parent directory to continue search
		$start_dir = dirname($start_dir); 
	}
	while(index($start_dir, $end_dir) != -1);
	
	return "";
}
